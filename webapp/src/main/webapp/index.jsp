<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
     <link href="css/roboto.fonts.google/fonts.googleapis.com.roboto.css" rel="stylesheet">
    <link href="css/index.min.css" rel="stylesheet">
    <title>Login Application</title>
</head>
<body>
    <div id="app" >
        <v-app id="inspire">
         	<v-navigation-drawer clipped fixed app v-model="drawer" >
                <v-list   >
                    <v-list-tile href="#/envs" href="">
                        <v-list-tile-action><v-icon>dashboard</v-icon></v-list-tile-action>
                        <v-list-tile-content>
                            <v-list-tile-title>Inicio</v-list-tile-title>
                        </v-list-tile-content>
                    </v-list-tile>
	            </v-list>
	        </v-navigation-drawer>
			<v-toolbar dark color="lime"  fixed app clipped-left>
			    <v-toolbar-side-icon @click.stop="drawer = !drawer">
			    </v-toolbar-side-icon><v-toolbar-title>Login Application</v-toolbar-title>
			    <v-spacer></v-spacer>
			    <v-toolbar-items class="hidden-sm-and-down">
			        <v-btn icon><v-icon>refresh</v-icon></v-btn>
			    </v-toolbar-items>
			</v-toolbar>
			<v-content  >
			    <v-fade-transition  mode="out-in">
				    <router-view></router-view>
				</v-fade-transition>
			</v-content>
			<v-footer v-show="logged===true">
			    &nbsp; DevOps (Powered by Indra)
			</v-footer>


			<v-dialog v-model="dialog.visible" width="550">
			    <v-card dense>
			        <v-card-title class="headline" primary-title >
			            Acceso denegado
			        </v-card-title>
			        <v-card-text>
			            <p>Usuario o contraseña incorrecta</p>
			        </v-card-text>
			        <v-card-actions>
			            <v-spacer></v-spacer>
			            <v-btn @click="dialog.visible = false">Aceptar</v-btn>
			        </v-card-actions>
			    </v-card>
			</v-dialog>


<script type="text/x-template" id="home" >
    <v-container  grid-list-md fluid >
        <v-layout >
            <v-flex xs12 >
                <v-card dense >
                    <v-card-title>
                        <h2>¡Bienvenido al sistema!</h2>
                    </v-card-title>
                    <v-card-text>
						<p>Si estas viendo &eacute;ste mensaje quiere decir que iniciaste sesi&oacute;n correctamente!</p>
					</v-card-text>
                    
                </v-card>
            </v-flex>    
        </v-layout>
    </v-container>
</script>


<script type="text/x-template" id="settings" >
    <v-container  grid-list-md fluid >
        <v-layout >
            <v-flex xs12 >
                <v-card dense >
                    <v-card-text  >
                        text
                    </v-card-text>
                    <v-separator></v-separator>
                    <v-card-actions>
                        <v-spacer></v-spacer>
                        <v-btn color="primary" >Aplicar configuración</v-btn>
                    </v-card-actions>
                </v-card>
            </v-flex>    
        </v-layout>
    </v-container>
</script>


<script type="text/x-template" id="login" >
	<v-container  fluid fill-height >
	    <v-layout align-center justify-center >
	    	<v-flex xs12 sm8 md4 >
		    	<v-card class="elevation-12" >
		    	<v-toolbar color="lime">
		    		<v-toolbar-title color="white"  >
		    			Iniciar Sesi&oacute;n
					</v-toolbar-title>
		
				</v-toolbar>
		<v-card-text  >
		    <v-form  >
		    <v-text-field  v-model="email" prepend-icon="person" label="Email" >
		    
		</v-text-field>
		<v-text-field type="password" v-model="password" prepend-icon="person" label="Password" >
		    
		</v-text-field>
		
		</v-form>
		
		</v-card-text>
		<v-card-actions >
		    <v-spacer></v-spacer><v-btn color="lime" @click="login" >
		    Iniciar sesión
		</v-btn>
		
		</v-card-actions>
		
		</v-card>
		
		</v-flex>
		
		</v-layout>
	
	</v-container>

</script>
	
</v-app>
    </div>
    <script src="js/index.min.js"></script>
    <script src="js/axios.min.js"></script>
    <script src="js/index.js"></script>
</body>
</html>