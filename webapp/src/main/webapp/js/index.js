
//Home component
var home = Vue.component('home-page', {
    template: '#home',
    data: function() { return {"message":"Hay historias!!!"};},created: function (){
        if (this.$root.logged !== true) {
            router.push({
                name: 'login'
            });
        }
    },methods: {
        showDeck: function (){
            this.$root.dlgDeck = true;
        },reveal: function (){
            this.$root.socket.emit('reveal', null);
        },
        listArtifacts: function(dir){
            router.push({
                name: 'artifacts',
                params: {
                    r: dir
                }
            });
        },
    },mounted: function (){},
});
//settings component
var Settings = Vue.component('settings-page', {
    template: '#settings',
    data: function() { 
        return {
            "message":"Settings page!",
        };
    },
    methods: {
        
    },mounted: function (){
        
    },
});

//Login component
var login = Vue.component('login-page', {
    template: '#login',
    data: function() { 
    	return {
    		"message":"login component!",
    		"password":"",
    		"email":""
			};
		},
		methods: {
			login: function (){
				var self = this;
				// console.log('user: ', this.username);
	            //this.$root.logged = true;
				axios.get('controller.jsp?email='+this.email)
				  .then(function (response) {
				    // handle success
				    console.log(response.data);
				    if(response.data.success === "true"){
				    	router.push({
			                name: 'home'
			            });
				    }else{
				    	self.$root.dialog.visible = true;
				    }
				    
				  })
				  .catch(function (error) {
				    // handle error
				    console.log(error);
				  })
				  .finally(function () {
				    // always executed
				  });

	            
	            
	        },
        },
        mounted: function (){
        	
        },
});

//routes
var routes = [
    { 
        path: '/home', 
        name: 'home', 
        component: home 
    },
    
    { 
        path: '/settings', 
        name: 'settings', 
        component: Settings 
    },
    
    { 
        path: '/login', 
        name: 'login', 
        component: login 
    }
    ,{
        path: '*',
        redirect: {
            name: 'login'
        }
    }
];

//router
var router = new VueRouter({
    routes: routes,
    root: '/login'
});

//Vue App
new Vue(
    { 
        el: '#app',
        router: router, 
        data: {
            "drawer":true,

            "logged":true,
            
            'dialog': {
                visible: false,
            },
        },
        created: function (){},
        methods: {
            
        },
        
        mounted: function (){
            

        },
    }
);


